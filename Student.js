import {Person} from "./Person";

class Student extends Person {
    constructor(hoTen, ngaySinh, queQuan, tenTruong, lop, soDienThoai) {
    super(hoTen, ngaySinh, queQuan);
    this.tenTruong = tenTruong;
    this.lop = lop;
    this.soDienThoai = soDienThoai;
    }
}

export {Student}