import { Student } from "./Student";

class SinhVien extends Student {
    constructor(hoTen, ngaySinh, queQuan, tenTruong, lop, soDienThoai, chuyenNganh, MSSV) {
        super(hoTen, ngaySinh, queQuan, tenTruong, lop, soDienThoai)
        this.MSSV = MSSV;
        this.chuyenNganh = chuyenNganh;
    }
}

export {SinhVien}